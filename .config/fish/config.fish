if status is-interactive
    set fish_greeting
    set -x XDG_CACHE_HOME $HOME/.cache
    set -x XDG_CONFIG_HOME $HOME/.config
    set -x XDG_DATA_HOME $HOME/.local/share
    set -x XDG_STATE_HOME $HOME/.local/state
    fish_add_path $HOME/.dotfiles/.local/bin
    fish_add_path $HOME/.local/bin
    
    # Elixir
    set -x MIX_XDG true
    
    # GPG
    set -x GPG_TTY (tty)

    # SSH Agent
    set -x SSH_AUTH_SOCK $XDG_RUNTIME_DIR/ssh-agent.socket
    
    # Rust
    set -x CARGO_HOME $XDG_DATA_HOME/cargo
    set -x RUSTUP_HOME $XDG_DATA_HOME/rustup

    # Set default editor
    set -gx EDITOR micro

    # Set VA-API driver
    set -gx LIBVA_DRIVER_NAME iHD

    # Set Fcitx5 IM
    #set -gx GTK_IM_MODULE fcitx
    #set -gx QT_IM_MODULE fcitx
    #set -gx GLFW_IM_MODULE ibus
    #set -gx SDL_IM_MODULE fcitx
    #set -gx XMODIFIERS @im=fcitx

    # Activate rtx
    ~/.local/share/rtx/bin/rtx activate fish | source
end

# Aliases
alias uvolt="sudo intel-undervolt"
alias dturbop="echo 1 | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo"
alias dturbo="echo 0 | sudo tee /sys/devices/system/cpu/cpufreq/boost"
alias eturbop="echo 0 | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo"
alias eturbo="echo 1 | sudo tee /sys/devices/system/cpu/cpufreq/boost"
alias nightmode="redshift -P -O 3600 -b 0.6"
alias daymode="redshift -x"
alias l="lsd -al"
alias ls="lsd"
alias cat="bat"
alias mobttl="sudo sysctl -w net.ipv4.ip_default_ttl=63"
alias gitbc="git fetch --prune; git branch -v | grep gone | cut -f3 -d' ' | xargs git branch -D"


